class CreateOrganizations < ActiveRecord::Migration[6.1]
  def change
    create_table :organizations, id: :uuid do |t|
      t.string :name
      t.string :slug
      t.references :universe, null: false, foreign_key: true, type: :uuid

      t.timestamps
    end
    add_index :organizations, :slug
    add_index :organizations, [:universe_id, :slug], unique: true
  end
end

class CreateJournals < ActiveRecord::Migration[6.1]
  def change
    create_table :journals, id: :uuid do |t|
      t.references :organization, null: false, foreign_key: true, type: :uuid
      t.references :person, null: false, foreign_key: true, type: :uuid
      t.date :done_at
      t.boolean :private, default: false

      t.timestamps
    end
  end
end

class AddKindToTodo < ActiveRecord::Migration[6.1]
  def change
    add_column :todos, :kind, :string
  end
end

class CreateTodos < ActiveRecord::Migration[6.1]
  def change
    create_table :todos, id: :uuid do |t|
      t.string :title
      t.integer :iid
      t.references :organization, null: false, foreign_key: true, type: :uuid
      t.boolean :done

      t.timestamps
    end
    add_index :todos, [:organization_id, :iid], unique: true
  end
end

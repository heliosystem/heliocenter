class CreateTodoDependencies < ActiveRecord::Migration[6.1]
  def change
    create_table :todo_dependencies, id: :uuid do |t|
      t.uuid :dependent_id, null: true, foreign_key: true
      t.uuid :independent_id, null: true, foreign_key: true

      t.timestamps
    end
    
  end
end

class CreateTodoAssignments < ActiveRecord::Migration[6.1]
  def change
    create_table :todo_assignments, id: :uuid do |t|
      t.references :todo, null: false, foreign_key: true, type: :uuid
      t.references :person, null: false, foreign_key: true, type: :uuid
      t.string :role

      t.timestamps
    end
  end
end

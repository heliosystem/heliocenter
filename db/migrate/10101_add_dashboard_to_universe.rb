class AddDashboardToUniverse < ActiveRecord::Migration[6.1]
  def change
    add_column :universes, :dashboard, :text
  end
end

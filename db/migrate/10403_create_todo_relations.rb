class CreateTodoRelations < ActiveRecord::Migration[6.1]
  def change
    create_table :todo_relations, id: :uuid do |t|
      t.uuid :parent_id, null: false, foreign_key: true
      t.uuid :child_id, null: false, foreign_key: true

      t.timestamps
    end
  end
end

class CreateMilestones < ActiveRecord::Migration[6.1]
  def change
    create_table :milestones, type: :uuid do |t|
      t.string :name
      t.string :slug
      t.date :due_at
      t.references :organization, null: false, foreign_key: true, type: :uuid

      t.timestamps
    end
    add_index :milestones, :slug
    add_index :milestones, [:organization_id, :slug], unique: true
  end
end

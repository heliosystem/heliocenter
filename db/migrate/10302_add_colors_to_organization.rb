class AddColorsToOrganization < ActiveRecord::Migration[6.1]
  def change
    add_column :organizations, :fg, :string
    add_column :organizations, :bg, :string
  end
end

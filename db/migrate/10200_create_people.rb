class CreatePeople < ActiveRecord::Migration[6.1]
  def change
    create_table :people, id: :uuid do |t|
      t.string :name
      t.references :universe, null: false, foreign_key: true, type: :uuid
      t.string :slug

      t.timestamps
    end
    add_index :people, :slug
    add_index :people, [:universe_id, :slug], unique: true
  end
end

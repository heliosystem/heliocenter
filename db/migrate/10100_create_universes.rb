class CreateUniverses < ActiveRecord::Migration[6.1]
  def change
    create_table :universes, id: :uuid do |t|
      t.string :name
      t.string :slug
      t.string :fg
      t.string :bg

      t.timestamps
    end
    add_index :universes, :slug, unique: true
  end
end

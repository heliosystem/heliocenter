class CreateEvents < ActiveRecord::Migration[6.1]
  def change
    create_table :events, id: :uuid do |t|
      t.references :organization, null: false, foreign_key: true, type: :uuid
      t.string :title
      t.string :slug
      t.string :mode
      t.datetime :starts_at
      t.datetime :ends_at
      t.string :location
      t.integer :limit
      t.boolean :locked

      t.timestamps
    end
    add_index :events, :slug
    add_index :events, [:organization_id, :slug], unique: true
  end
end

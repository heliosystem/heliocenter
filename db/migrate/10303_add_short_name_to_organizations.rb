class AddShortNameToOrganizations < ActiveRecord::Migration[6.1]
  def change
    add_column :organizations, :short, :string
    add_index :organizations, [:universe_id, :short], unique: true
  end
end

class AddDashboardToOrganization < ActiveRecord::Migration[6.1]
  def change
    add_column :organizations, :dashboard, :text
  end
end

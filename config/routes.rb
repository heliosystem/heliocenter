Rails.application.routes.draw do
  root to: 'welcome#index'

  post '/auth/:provider/callback' => 'sessions#create'
  get '/auth/destroy' => 'sessions#destroy', as: 'destroy_session'

  resources :universes, only: [:index, :new, :create] do
    get 'preview'
  end

  resources :universes, path: '/' do
    get 'calendar', on: :member

    resources :people do
      get 'avatar', on: :member
      get 'generic_avatar', on: :member
    end

    resources :organizations, path: '/o' do
      get 'calendar', on: :member
      resources :milestones
      resources :journals
      resources :events
      resources :todos do
        post 'relate', on: :collection
        post 'unrelate', on: :collection
        post 'assign', on: :member
        post 'unassign', on: :member
      end
    end
  end
end

OmniAuth.config.allowed_request_methods = [:post, :get]

Rails.application.config.middleware.use OmniAuth::Builder do
  provider :developer, fields: [:email, :name] # unless Rails.env.production?
end

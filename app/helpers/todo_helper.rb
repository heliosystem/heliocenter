module TodoHelper
  def todos_to_nodes(todos)
    todos.map do |t|
      { 
        id: t.id,
        label: "#{t.title}#{todo_counter(t)}",
        shape: todo_node_shape(t),
        url: url_for(container_route_for(t)),
        color: todo_node_color(t),
        font: todo_node_font(t),
        borderWidth: todo_border_width(t),
        image: todo_image(t)
      }
    end.to_json.html_safe
  end

  def todos_to_edges(todos)
    todos.map do |t|
      t.dependents.map do |d|
        {
          to: t.id,
          from: d.id,
          arrows: {
            from: {
              enabled: true,
              type: 'arrow'
            }
          }
        }
      end + 
      t.children.map do |d|
        {
          from: t.id,
          to: d.id,
          arrows: {
            to: {
              enabled: true,
              type: 'circle'
            },
            from: {
              enabled: true,
              type: 'arrow'
            }
          },
          dashes: true,
          color: '#999999'
        }
      end
    end.flatten.to_json.html_safe
  end

  def todo_node_color todo
    if todo.done && !todo.independent_tasks.any? { |t| !t.done }
      '#395'
    elsif todo.overdue?
      '#e40'
    elsif todo.blocked?
      '#d82'
    else
      if todo.kind == 'Epic'
        '#fd1'
      else
        '#8ee'
      end
    end
  end

  def todo_counter todo
    if todo.kind == 'Epic'
      if todo.children.present?
        "\n" + todo.children.where(done: true).count.to_s +
        ' / ' +
        todo.children.count.to_s
      end
    else
      if todo.independent_tasks.present?
        "\n" + todo.independent_tasks.where(done: true).count.to_s + 
        ' / ' + 
        todo.independent_tasks.count.to_s
      end
    end
  end

  def todo_node_shape todo
    if todo.kind == 'Epic'
      mgr = todo.assignments.where(role: 'managing').first&.person
      if mgr
        'circularImage'
      else
        'ellipse'
      end
    else
      'box'
    end
  end

  def todo_image todo
    if todo.kind == 'Epic' 
      mgr = todo.assignments.where(role: 'managing').first&.person
      if mgr
        avatar_universe_person_path(mgr.universe, mgr)
      else
        nil
      end
    else
      nil
    end
  end

  def todo_node_font todo
    spec = {}
    spec['size'] = if todo.kind == 'Epic'
      16
    else
      12
    end

    if todo.blocked? && todo.kind != 'Epic'
      spec['color'] = 'white'
    end

    if todo.overdue? && todo.kind != 'Epic'
      spec['color'] = '#fcc'
    end

    spec
  end

  def todo_border_width todo
    if todo.ready?
      if todo.kind == 'Epic'
        8
      else
        2
      end
    else
      if todo.kind == 'Epic'
        6
      else
        2
      end
    end
  end

  def todo_text todo
    "#{todo.kind} #{todo.key}"
  end

  def todo_link todo
    txt = "#{todo_text(todo)} #{todo.title}"
    if todo.done
      txt = "<strike>#{txt}</strike>"
    end
    link_to txt.html_safe, container_route_for(todo)
  end
end
module ApplicationHelper
  def contrasting_color(color)
    base = Color::RGB.by_hex(color)
    if base.brightness > 0.5
      '#000'
    else
      '#fff'
    end
  end

  def action_link path, icon, text, bg, fg, opts={}
    options = {
      style: "background-color: #{bg}; color: #{fg}", 
      title: text
    }.merge opts
    link_to icon.html_safe, path, options
  end
end

module UniverseHelper
  def universe_fg_color
    current_universe&.fg || '#fff'
  end

  def universe_bg_color
    current_universe&.bg || '#7e1894'
  end
end
module OrganizationHelper
  def organization_fg_color
    current_organization&.fg || universe_bg_color || '#fff'
  end

  def organization_bg_color
    current_organization&.bg || universe_fg_color || '#7e1894'
  end
end
class TodosController < SubapplicationController
  before_action :set_todo, only: %i[ show edit update destroy assign unassign ]

  # GET /todos or /todos.json
  def index
    @todos = current_organization.todos
  end

  # GET /todos/1 or /todos/1.json
  def show
  end

  # GET /todos/new
  def new
    @todo = current_organization.todos.new
  end

  # GET /todos/1/edit
  def edit
  end

  # POST /todos or /todos.json
  def create
    @todo = current_organization.todos.new(todo_params)

    respond_to do |format|
      if @todo.save
        format.html { redirect_to current_container_route_for(@todo), notice: "Todo was successfully created." }
        format.json { render :show, status: :created, location: @todo }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @todo.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /todos/1 or /todos/1.json
  def update
    respond_to do |format|
      if @todo.update(todo_params)
        format.html { redirect_to current_container_route_for(@todo), notice: "Todo was successfully updated." }
        format.json { render :show, status: :ok, location: @todo }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @todo.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /todos/1 or /todos/1.json
  def destroy
    @todo.destroy
    respond_to do |format|
      format.html { redirect_to current_container_route_for(:todos), notice: "Todo was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  def relate
    target = nil
    if params[:dependent_id].present? && params[:independent_id].present?
      dependent = current_universe.todos.find(params[:dependent_id])
      target = independent = current_universe.todos.find(params[:independent_id])
      TodoDependency.create!(dependent_id: dependent.id, independent_id: independent.id)
    elsif params[:parent_id].present? && params[:child_id].present?
      target = parent = current_universe.todos.find(params[:parent_id])
      child = current_universe.todos.find(params[:child_id])
      TodoRelation.create!(parent_id: parent.id, child_id: child.id)
    else
      if request.xhr?
        "Fail"
      else
        redirect_back(fallback_location: current_container_route_for(:todos))
      end
      return
    end
    if request.xhr?
      "OK"
    else
      redirect_back(fallback_location: container_route_for(target))
    end
  end

  def unrelate
    target = nil
    if params[:dependent_id].present? && params[:independent_id].present?
      dependent = current_universe.todos.find(params[:dependent_id])
      target = independent = current_universe.todos.find(params[:independent_id])
      TodoDependency.where(dependent_id: dependent.id, independent_id: independent.id).each(&:destroy)
    elsif params[:parent_id].present? && params[:child_id].present?
      target = parent = current_universe.todos.find(params[:parent_id])
      child = current_universe.todos.find(params[:child_id])
      TodoRelation.where(parent_id: parent.id, child_id: child.id).each(&:destroy)
    else
      if request.xhr?
        "Fail"
      else
        redirect_back(fallback_location: current_container_route_for(:todos))
      end
      return
    end
    if request.xhr?
      "OK"
    else
      redirect_back(fallback_location: container_route_for(target))
    end
  end

  def assign
    person = current_universe.people.find(params[:person_id])
    TodoAssignment.create(todo: @todo, person: person, role: params[:role])
    if request.xhr?
      "OK"
    else
      redirect_back(fallback_location: current_container_route_for(@todo))
    end
  end

  def unassign
    person = current_universe.people.find(params[:person_id])
    TodoAssignment.where(todo: @todo, person: person).each(&:destroy)
    if request.xhr?
      "OK"
    else
      redirect_back(fallback_location: current_container_route_for(@todo))
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_todo
      @todo = current_organization.todos.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def todo_params
      params.require(:todo).permit(:title, :description, :done, :kind, :due_at)
    end
end

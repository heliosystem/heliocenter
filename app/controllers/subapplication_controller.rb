class SubapplicationController < ApplicationController
  before_action :set_universe, :set_organization
  before_action :check_for_belonging_or_super_admin

  def set_universe
    @current_universe = Universe.friendly.find(params[:universe_id])
  end

  def set_organization
    @current_organization ||= if params[:organization_id]
      current_universe.organizations.friendly.find(params[:organization_id])
    else
      nil
    end
  end

  def current_organization
    @current_organization
  end

  def current_container
    current_organization || current_universe || nil
  end

  def current_container_route_array
    if current_organization
      [current_universe, current_organization]
    elsif current_universe
      [current_universe]
    else
      []
    end
  end

  def current_container_route_array_for(resource)
    current_container_route_array.push(resource)
  end

  def current_container_route_for(target, method=nil, *args)
    if target.is_a? ApplicationRecord
      if method
        route = method ? method.to_s + '_' : ''  
        if current_organization
          send(route + "universe_organization_#{target.class.to_s.underscore}_path", current_universe, current_organization, target, *args)
        elsif current_universe
          send(route + "universe_#{target.class.to_s.underscore}_path", current_universe, target, *args)
        else
          send(route + target + '_path', *args)
        end
      else
        current_container_route_array_for(target)
      end
    elsif target.is_a? Symbol
      route = method ? method.to_s + '_' : ''
      if current_organization
        send(route + "universe_organization_#{target}_path", current_universe, current_organization, args)
      elsif current_universe
        send(route + "universe_#{target}_path", current_universe, *args)
      else
        send(route + target + '_path')
      end
    end
  end

  helper_method :current_organization, :current_container, :current_container_route_array_for, :current_container_route_for
end
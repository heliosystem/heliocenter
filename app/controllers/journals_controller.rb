class JournalsController < SubapplicationController
  before_action :set_journal, only: %i[ show edit update destroy ]

  # GET /journals or /journals.json
  def index
    @journals = if params[:date].present?
        current_organization.journals.where(done_at: params[:date])
      else
        current_organization.journals
      end.order('done_at DESC, created_at ASC')
  end

  # GET /journals/1 or /journals/1.json
  def show
  end

  # GET /journals/new
  def new
    @journal = current_organization.journals.new
  end

  # GET /journals/1/edit
  def edit
  end

  # POST /journals or /journals.json
  def create
    @journal = current_organization.journals.new(journal_params)

    respond_to do |format|
      if @journal.save
        format.html { redirect_to current_container_route_for(@journal), notice: "Journal was successfully created." }
        format.json { render :show, status: :created, location: @journal }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @journal.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /journals/1 or /journals/1.json
  def update
    respond_to do |format|
      if @journal.update(journal_params)
        format.html { redirect_to current_container_route_for(@journal), notice: "Journal was successfully updated." }
        format.json { render :show, status: :ok, location: @journal }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @journal.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /journals/1 or /journals/1.json
  def destroy
    @journal.destroy
    respond_to do |format|
      format.html { redirect_to current_container_route_for(:journals), notice: "Journal was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_journal
      @journal = current_organization.journals.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def journal_params
      params.require(:journal).permit(:person_id, :done_at, :private, :entry)
    end
end

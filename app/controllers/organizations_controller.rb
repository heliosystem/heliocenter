class OrganizationsController < SubapplicationController
  before_action :set_organization, only: %i[ show calendar edit update destroy ]

  # GET /organizations or /organizations.json
  def index
    @organizations = current_universe.organizations.all
  end

  # GET /organizations/1 or /organizations/1.json
  def show
  end

  def calendar
    respond_to do |format|
        format.html {}
        format.json do
          @todos = current_organization.todos.where.not(due_at: nil).where('due_at >= ? AND due_at <= ?', params[:start], params[:end])
          @milestones = current_organization.milestones.where('due_at >= ? AND due_at <= ?', params[:start], params[:end])
          @events = current_organization.events.where('ends_at >= ? AND starts_at <= ?', params[:start], params[:end])
          @journals = current_organization.journals.where(private: false).where('done_at >= ? AND done_at <= ?', params[:start], params[:end]).pluck(:done_at).uniq
        end
    end
  end

  # GET /organizations/new
  def new
    @organization = current_universe.organizations.new
  end

  # GET /organizations/1/edit
  def edit
  end

  # POST /organizations or /organizations.json
  def create
    @organization = current_universe.organizations.new(organization_params)

    respond_to do |format|
      if @organization.save
        format.html { redirect_to [current_universe, @organization], notice: "Organization was successfully created." }
        format.json { render :show, status: :created, location: @organization }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @organization.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /organizations/1 or /organizations/1.json
  def update
    respond_to do |format|
      if @organization.update(organization_params)
        format.html { redirect_to [current_universe, @organization], notice: "Organization was successfully updated." }
        format.json { render :show, status: :ok, location: @organization }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @organization.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /organizations/1 or /organizations/1.json
  def destroy
    @organization.destroy
    respond_to do |format|
      format.html { redirect_to universe_organizations_url(current_universe), notice: "Organization was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_organization
      @current_organization = @organization = Organization.friendly.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def organization_params
      params.require(:organization).permit(:name, :description, :dashboard, :fg, :bg, :short)
    end
end

class PeopleController < SubapplicationController
  before_action :set_person, only: %i[ show edit update destroy avatar generic_avatar]

  # GET /people or /people.json
  def index
    @people = current_universe.people
  end

  # GET /people/1 or /people/1.json
  def show
  end

  # GET /people/new
  def new
    @person = current_universe.people.new
  end

  # GET /people/1/edit
  def edit
  end

  # POST /people or /people.json
  def create
    @person = current_universe.people.new(person_params)

    respond_to do |format|
      if @person.save
        format.html { redirect_to current_container_route_for(@person), notice: "Person was successfully created." }
        format.json { render :show, status: :created, location: @person }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @person.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /people/1 or /people/1.json
  def update
    respond_to do |format|
      if @person.update(person_params)
        format.html { redirect_to current_container_route_for(@person), notice: "Person was successfully updated." }
        format.json { render :show, status: :ok, location: @person }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @person.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /people/1 or /people/1.json
  def destroy
    @person.destroy
    respond_to do |format|
      format.html { redirect_to universe_people_url(current_universe), notice: "Person was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  def avatar
    if @person.avatar.attached?
      redirect_to url_for(@person.avatar)
    else
      redirect_to current_container_route_for(@person, :generic_avatar, format: :svg)
    end
  end

  def generic_avatar
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_person
      @person = current_universe.people.friendly.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def person_params
      params.require(:person).permit(:name, :avatar, :email)
    end
end

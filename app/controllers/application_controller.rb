class ApplicationController < ActionController::Base
  before_action :check_for_user
  around_action :set_time_zone

  def current_user
    @current_user ||= if session[:user_id]
      User.find(session[:user_id])
    else
      nil
    end
  end

  def set_time_zone(&block)
    Time.use_zone('EST', &block)
  end

  def super_admin?
    current_user && current_user.super_admin
  end

  def check_for_user
    redirect_to root_path, error: 'Please log in first.' unless current_user
  end

  def check_for_super_admin
    redirect_to root_path, error: "You can't go there." unless super_admin?
  end

  def check_for_belonging
    redirect_to root_path, error: "You can't go there." unless Belonging.find_by(user: current_user, universe: current_universe)
  end

  def check_for_belonging_or_super_admin
    redirect_to root_path, error: "You can't go there." unless super_admin? || Person.find_by(user: current_user, universe: current_universe)
  end

  def current_universe
    @current_universe ||= if params[:universe_id]
      Universe.friendly.find(params[:universe_id])
    else
      nil
    end
  end

  def current_organization
    @current_organization || @organization || nil
  end

  def container_route_array_for(target)
    if target['container_type'].present?
      if target.container_type == 'Organization'
        [target.container.universe, target.container, target]
      elsif target.container_type == 'Universe'
        [target.container, target]
      else
        []
      end
    elsif target.try(:organization_id)
      container_route_array_for(target.organization) << target
    elsif target.try(:universe_id)
      [target.universe, target]
    end
  end

  def container_route_for(target, method=nil, *args)
    if target.is_a? ApplicationRecord
      if method
        route = method ? method.to_s + '_' : ''  
        if target.container_type == 'Organization'
          send(route + "universe_organization_#{target.class.to_s.underscore}_path", target.container.universe, target.container, target, *args)
        elsif target.container_type == 'Universe'
          send(route + "universe_#{target.class.to_s.underscore}_path", target.container, target, *args)
        else
          send(route + target + '_path', *args)
        end
      else
        container_route_array_for(target)
      end
    end
  end

  helper_method :current_universe, :current_organization, :container_route_for, 
                :container_route_array_for, :current_user, :super_admin?
end

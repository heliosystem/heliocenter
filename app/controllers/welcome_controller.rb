class WelcomeController < ApplicationController
  skip_before_action :check_for_user

  def index
    if current_user
      @universes = Universe.all
      render :dashboard
    end
  end
end

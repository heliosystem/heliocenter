json.extract! organization, :id, :name, :slug, :universe, :created_at, :updated_at
json.url organization_url(organization, format: :json)

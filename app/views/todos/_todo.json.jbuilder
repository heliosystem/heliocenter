json.extract! todo, :id, :title, :container_id, :container_type, :done, :created_at, :updated_at
json.url todo_url(todo, format: :json)

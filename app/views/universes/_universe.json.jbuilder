json.extract! universe, :id, :name, :slug, :fg, :bg, :created_at, :updated_at
json.url universe_url(universe, format: :json)

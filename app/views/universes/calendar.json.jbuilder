json.array! @todos.each do |todo|
  json.start todo.due_at
  json.end todo.due_at
  json.allDay true
  json.title "#{todo.kind} #{todo.key}: " + todo.title
  json.url url_for(container_route_for(todo))
  json.color todo_node_color(todo)
  json.textColor contrasting_color(todo_node_color(todo))
end

json.array! @milestones.each do |milestone|
  json.start milestone.due_at
  json.end milestone.due_at
  json.allDay true
  json.title milestone.name
  json.url url_for(container_route_for(milestone))
  json.color "#444"
end

json.array! @events.each do |event|
  json.start event.starts_at
  json.end event.ends_at
  json.title event.title
  json.url url_for(container_route_for(event))
  json.color "#7e1894"
end

json.array! @journals.each do |journal|
  org = Organization.find(journal[1])
  json.start journal[0]
  json.end journal[0]
  json.allDay true
  json.title "#{org.name} Journal Entries"
  json.url universe_organization_journals_url(current_universe, org, date: journal[0])
  json.color '#5a3703'
end
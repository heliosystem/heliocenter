json.extract! journal, :id, :organization_id, :person_id, :done_at, :private, :created_at, :updated_at
json.url journal_url(journal, format: :json)

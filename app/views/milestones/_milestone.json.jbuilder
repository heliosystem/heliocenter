json.extract! milestone, :id, :name, :due_at, :organization_id, :created_at, :updated_at
json.url milestone_url(milestone, format: :json)

json.extract! event, :id, :organization_id, :title, :slug, :mode, :starts_at, :ends_at, :location, :limit, :locked, :created_at, :updated_at
json.url event_url(event, format: :json)

class Person < ApplicationRecord
  include FriendlyId
  friendly_id :name, use: :slugged
  belongs_to :universe
  belongs_to :user, optional: true
  has_many :journals
  has_one_attached :avatar

  attr_accessor :email
  before_save :assign_user

  def initials
    words = name.split(' ')
    words.first[0] + words.last[0]
  end

  def assignments
    TodoAssignment.includes(:todo).where(person_id: id)
  end

  private

  def assign_user
    if email.present?
      self.user = User.find_by(email: email)
    end
  end
end

class Journal < ApplicationRecord
  has_rich_text :entry

  belongs_to :organization
  belongs_to :person

  
end

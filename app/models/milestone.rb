class Milestone < ApplicationRecord
  extend FriendlyId
  friendly_id :url_name, :use => :scoped, :scope => :organization_id
  belongs_to :organization

  def url_name
    due_at.strftime("%Y-%-m-%-d ") + name
  end
end

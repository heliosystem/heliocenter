class Event < ApplicationRecord
  extend FriendlyId
  friendly_id :url_name, :use => :scoped, :scope => :organization_id
  belongs_to :organization

  def url_name
    starts_at.strftime("%Y-%-m-%-d ") + title
  end

  def one_day?
    starts_at.beginning_of_day == ends_at.beginning_of_day
  end

  def status_message
    if ends_at < Time.now
      "Done"
    elsif starts_at < Time.now
      "Happening Now"
    elsif locked
      "Locked"
    else
      "Open"
    end
  end
end

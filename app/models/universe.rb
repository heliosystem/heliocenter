class Universe < ApplicationRecord
  include Dashboardable
  include FriendlyId
  friendly_id :name, use: :slugged
  has_rich_text :description

  has_many :organizations
  has_many :todos, through: :organizations
  has_many :milestones, through: :organizations
  has_many :journals, through: :organizations
  has_many :events, through: :organizations
  has_many :people
  has_many :users, through: :people

  def ready_tasks
    organizations.map(&:ready_tasks).flatten
  end
end

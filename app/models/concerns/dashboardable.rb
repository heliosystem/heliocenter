require 'csv'
module Dashboardable
  extend ActiveSupport::Concern

  def dashboard_config
    CSV.parse(dashboard)
  end
end
class Organization < ApplicationRecord
  include Dashboardable
  include FriendlyId
  friendly_id :name, use: :slugged
  has_rich_text :description

  belongs_to :universe
  has_many :todos
  has_many :milestones
  has_many :journals
  has_many :events

  def ready_tasks
    todos.tasks.to_a.keep_if do |t|
      t.ready?
    end
  end
end

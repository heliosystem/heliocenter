class Todo < ApplicationRecord
  has_rich_text :description

  belongs_to :organization
  belongs_to :todo_assignments, dependent: :destroy, optional: true
  belongs_to :todo_relation, dependent: :destroy, optional: true
  belongs_to :todo_dependency, dependent: :destroy, optional: true

  before_validation :generate_iid

  def self.KINDS 
    ['Epic', 'Task']
  end

  def self.ROLES
    {
      'Epic' => [
        'managing',
        'helping'
      ],
      'Task' => [
        'doing',
        'reviewing'
      ]
    }
  end

  scope :epics, -> { where(kind: 'Epic') }
  scope :tasks, -> { where(kind: 'Task') }

  def assignments
    TodoAssignment.includes(:person).where(todo_id: id)
  end

  def people
    organization.universe.people.where(id: assignments.map(&:person_id))
  end

  def key
    organization.short + iid.to_s
  end

  def manager
    @manager ||= if assignments.where(role: 'managing').present?
      assignments.where(role: 'managing').first&.person
    elsif parents.present?
      parents.first.manager
    elsif independents.present?
      independents.first.manager
    else
      nil
    end
  end
  
  def dependents
    organization.universe.todos.where(id: TodoDependency.where(independent_id: id).pluck(:dependent_id))
  end

  def independents
    organization.universe.todos.where(id: TodoDependency.where(dependent_id: id).pluck(:independent_id))
  end

  def parents
    organization.universe.todos.where(id: TodoRelation.where(child_id: id).pluck(:parent_id))
  end

  def children
    organization.universe.todos.where(id: TodoRelation.where(parent_id: id).pluck(:child_id))
  end

  def related
    ids = [
      id,
      TodoDependency.where(dependent_id: id).pluck(:independent_id),
      TodoDependency.where(independent_id: id).pluck(:dependent_id),
      TodoRelation.where(parent_id: id).pluck(:child_id),
      TodoRelation.where(child_id: id).pluck(:parent_id)
    ].flatten.uniq
    Todo.where(id: ids)
  end

  def dependent_tasks
    dependents.where.not(kind: 'Epic')
  end

  def independent_tasks
    independents.where.not(kind: 'Epic')
  end

  def ready?
    @ready ||= !done && independents.all.to_a.all? { |t| t.done }
  end

  def blocked?
    !ready?
  end

  def overdue?
    @overdue ||= (due_at.present? && due_at < Date.today) ||
    children.any? { |c| c.overdue? } || 
    independents.any? { |c| c.overdue? }
  end

  private

  def generate_iid
    self.iid = (self.organization.todos.pluck(:iid).max || 0) + 1
  end
end

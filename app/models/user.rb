class User < ApplicationRecord
  has_many :people
  has_many :universes, through: :people

  def self.find_or_create_from_auth_hash(auth)
    oauth_email = auth["info"]["email"] 
    oauth_name = auth["info"]["name"] || auth["info"]["nickname"]
    self.where(email: oauth_email, name: oauth_name).first_or_create do |u|
      u.name = oauth_name
    end
  end
end
